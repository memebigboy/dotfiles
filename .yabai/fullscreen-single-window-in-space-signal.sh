#!/usr/bin/env bash

# TODO: use $YABAI_WINDOW_ID

if [ -z "$YABAI_WINDOW_ID" ]; then
   echo "YABAI_WINDOW_ID variable not provided" >&2; exit 1
fi

spaceId=$(yabai -m query --windows --window ${YABAI_WINDOW_ID} | jq '.space')

re='^[0-9]+$'
if ! [[ $spaceId =~ $re ]] ; then
   echo "Unable to find window" >&2; exit 1
fi

windowIds=($(yabai -m query --spaces --space ${spaceId} | jq -r '.windows | @sh'))
visibleWindows=()

for i in "${windowIds[@]}"
do
    isWindowVisible=$(yabai -m query --windows --window ${i} | jq '.visible')
    if [ "$isWindowVisible" == "1" ]; then
        visibleWindows+=($i)
    fi
done

if [ ${#visibleWindows[@]} -eq 1 ]; then
    # fullscreen ON
    echo "ON"

    if [ -f ~/.yabai/fullscreen-single-window-in-space-signal-cache-${spaceId}.txt ]; then
        echo "Cache already exists. Aborting..."; exit 0
    fi

    windowId=${visibleWindows[0]}
    hasBorder=$(yabai -m query --windows --window ${windowId} | jq '."border"')
    if [ "$hasBorder" == "1" ]; then
        yabai -m window --toggle border
    fi
    yabai -m space $spaceId --toggle padding
    
    saveContent="SPACEID=${spaceId}\nWINDOWID=${windowId}"
    echo -e ${saveContent} > ~/.yabai/fullscreen-single-window-in-space-signal-cache-${spaceId}.txt
else 
    # fullscreen OFF
    echo "OFF"
    
    if ! [ -f ~/.yabai/fullscreen-single-window-in-space-signal-cache-${spaceId}.txt ]; then
        echo "Cache file does not exist. Aborting..."; exit 0
    fi

    source ~/.yabai/fullscreen-single-window-in-space-signal-cache-${spaceId}.txt
    hasBorder=$(yabai -m query --windows --window ${WINDOWID} | jq '."border"')
    if [ "$hasBorder" == "0" ]; then
        yabai -m window --toggle border
    fi
    yabai -m space $SPACEID --toggle padding

    rm ~/.yabai/fullscreen-single-window-in-space-signal-cache-${spaceId}.txt
fi