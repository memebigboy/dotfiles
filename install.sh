#!/bin/zsh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Run Mac OS X custom configuration
# echo "Running Mac OS X custom configuration..."
# sh $DIR/osx.sh

# zshrc
if ! command -v zsh &> /dev/null; then
  echo "Installing Oh My Zsh..."
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
  rm -rf ~/.zshrc 
  ln -s $DIR/zsh/.zshrc ~/.zshrc
fi

# brew
if ! command -v brew &> /dev/null; then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# iterm2
ITERM=~/Library/Preferences/com.googlecode.iterm2.plist
# rm -rf $ITERM
if ! test -f "$ITERM"; then
  echo "Linking iterm2 plist file"
  ln $DIR/com.googlecode.iterm2.plist $ITERM
fi

# zsh
sh $DIR/zsh.sh

# Yabai & keyboard bindings
sh $DIR/yabai.sh

# Casks

casks=(
    # android-studio
    # android-sdk
    android-platform-tools
    #android-file-transfer
    openmtp
    visual-studio-code
    intellij-idea-ce
    #hyperdock
    homebrew/cask-drivers/logitech-options
    smooze
    postman
    1password
    iterm2
    transmission-remote-gui
    google-chrome
    # diffmerge
    the-unarchiver
    vlc
    mpv
    spotify
    slack
    plex
    steam
    sound-control
    mounty
    karabiner-elements
)

brew install --cask ${casks[@]}

binaries=(
    java
    maven
    gradle
    watch
    grep
    wget
    cmake
    htop-osx
    openssl
    automake
    fish
    scrcpy
)

brew install ${binaries[@]}
brew cleanup