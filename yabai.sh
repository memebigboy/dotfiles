#!/bin/zsh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#rm ~/.skhdrc
#rm ~/.yabairc

echo "Linking .skhdrc & .yabairc"
ln $DIR/.skhdrc ~/.skhdrc
ln $DIR/.yabairc ~/.yabairc

echo "Installing skhd"
brew install koekeishiya/formulae/skhd
brew services start skhd

echo "Installing yabai"
brew install koekeishiya/formulae/yabai
brew services start yabai

YABAI=~/.yabai
if ! test -f "$YABAI"; then
  echo "Linking .yabai folder"
  ln -s $DIR/.yabai $YABAI
fi

KARABINER=~/.config/karabiner
if ! test -f "$KARABINER"; then
  echo "Linking karabiner folder"
  ln -s $DIR/karabiner $KARABINER
fi
