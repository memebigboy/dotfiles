#!/bin/zsh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

OH_MY_ZSH=~/.oh-my-zsh
# rm -rf $OH_MY_ZSH

if ! test -f "$OH_MY_ZSH"; then
  echo "Linking .oh-my-zsh folder"
  ln -s $DIR/zsh/.oh-my-zsh $OH_MY_ZSH
fi

ZSHRC=~/.zshrc
# rm -rf $ZSHRC

if ! test -f "$ZSHRC"; then
  echo "Linking .zshrc"
  ln $DIR/zsh/.zshrc $ZSHRC
fi

P10K=~/.p10k.zsh
# rm -rf $P10K

if ! test -f "$P10K"; then
  echo "Linking .p10k.zsh"
  ln $DIR/zsh/.p10k.zsh $P10K
fi